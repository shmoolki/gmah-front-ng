///<reference path="../src/app/personne/domain/entities/Personne.ts"/>
import {PersonneHandler} from '../src/app/personne/Personne.handler';
import {PersonneLoader} from '../src/app/personne/domain/loader/PersonneLoader';
import {InMemoryPersonneLoader} from '../src/app/personne/adapters/secondaries/inmemory/InMemoryPersonne.loader';
import {Personne} from '../src/app/personne/domain/entities/Personne';
import {d} from '@angular/core/src/render3';

describe('personne Fetching information', () => {
  describe('a list ', () => {
    it('with zero personn if zero personne in data source', done => {
      const personneHandler = createPersonneHandler([]);
      personneHandler.all().subscribe(personnes => {
        expect(personnes).toEqual([]);
        done();
      });
    });

    it('with one personn if one personne in data source', done => {
      const samuelPersonne: Personne = new Personne(
        '1',
        'Mouyal',
        'Samuel',
        'Birnbaum 4',
        'Bnei Brak',
        '045444540' ,
        '',
        'samuelmouyal@gmail.com');
      const personneHandler = createPersonneHandler([samuelPersonne]);
      personneHandler.all().subscribe(personnes => {
        expect(personnes).toEqual([samuelPersonne]);
        done();
      });
    });

    it('with two personnes if two personnes in data source', done => {
      const samuelPersonne: Personne = new Personne(
        '1',
        'Mouyal',
        'Samuel',
        'Birnbaum 4',
        'Bnei Brak',
        '045444540' ,
        '',
        'samuelmouyal@gmail.com');
      const davidPersonne: Personne = new Personne(
        '2',
        'Levy',
        'David',
        'Yeroushalaim 22',
        'Bnei Brak',
        '0453454540' ,
        '',
        'david.levy@gmail.com');

      const personneHandler = createPersonneHandler([samuelPersonne, davidPersonne]);
      personneHandler.all().subscribe(personnes => {
        expect(personnes).toEqual([samuelPersonne, davidPersonne]);
        done();
      });
    });

  });

  it('a detail of one personne', done => {
    const samuelPersonne: Personne = new Personne(
      '1',
      'Mouyal',
      'Samuel',
      'Birnbaum 4',
      'Bnei Brak',
      '045444540' ,
      '',
      'samuelmouyal@gmail.com');
    const davidPersonne: Personne = new Personne(
      '2',
      'Levy',
      'David',
      'Yeroushalaim 22',
      'Bnei Brak',
      '0453454540' ,
      '',
      'david.levy@gmail.com');

    const personneHandler = createPersonneHandler([samuelPersonne, davidPersonne]);
    personneHandler.get('2').subscribe(personne => {
      expect(personne.number).toEqual(davidPersonne.number);
      done();
    });
  })

  function createPersonneHandler(personnePopulation: Personne[]) {
    const personneSource: PersonneLoader = new InMemoryPersonneLoader(personnePopulation);
    return new PersonneHandler(personneSource);
  }
});
