import {Observable} from 'rxjs';
import {Personne} from '../entities/Personne';

export interface PersonneLoader {
  all(): Observable<any>;

  get(number: string): Observable<Personne>;
}
