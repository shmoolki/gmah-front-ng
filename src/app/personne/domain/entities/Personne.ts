export class Personne {
  constructor(private _number: string,
              private firstName: string,
              private lastName: string,
              private adress: string,
              private city: string,
              private phone1: string,
              private phone2: string,
              private mail: string) {

  }


  get number(): string {
    return this._number;
  }
}
