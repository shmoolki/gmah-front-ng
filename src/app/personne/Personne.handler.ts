import {Observable, of} from 'rxjs';
import {PersonneLoader} from './domain/loader/PersonneLoader';
import {Personne} from './domain/entities/Personne';

export class PersonneHandler {
  constructor(private personneSource: PersonneLoader) {
  }


  all(): Observable<any> {
    return this.personneSource.all();
  }

  get(number: string): Observable<Personne> {
    return this.personneSource.get(number);
  }
}
