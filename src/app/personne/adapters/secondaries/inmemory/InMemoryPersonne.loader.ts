import {PersonneLoader} from '../../../domain/loader/PersonneLoader';
import {BehaviorSubject, Observable, of, Subject} from 'rxjs';
import {Personne} from '../../../domain/entities/Personne';
import {map} from 'rxjs/operators';

export class InMemoryPersonneLoader implements  PersonneLoader{
  private personnes$: Subject = new BehaviorSubject(this.personnes)
  constructor(private personnes: Personne[]) {
  }

  all(): Observable<any> {
    return this.personnes$;

  }

  get(number: string): Observable<Personne> {
    return this.personnes$.pipe(
      map(personnes => personnes.filter( personne => personne.number === number)[0])
    );
  }

}
